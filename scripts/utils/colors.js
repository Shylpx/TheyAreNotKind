define(function() {

  var Colors = {};

  (function(scope) {
    scope.WHITE = '#FFFFFF';
    scope.BLACK = '#000000';
    scope.RED = '0xE74C3C';
    scope.GREEN = '0x2ECC71';
  }(Colors));

  return Colors;
});
