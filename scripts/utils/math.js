define(function() {

  var MathUtils = {};

  (function(scope) {

    scope.distanceBetween2Points = function(a, b) {
      return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
    };
  }(MathUtils));

  return MathUtils;
});
