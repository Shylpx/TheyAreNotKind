define(
  ['phaser.min', 'states/boot', 'states/preload', 'states/menu', 'states/play', 'states/game_over'],
  function(_, BootState, PreloadState, MenuState, PlayState, GameOverState) {

  var Game = {};

  (function(scope) {
    scope.width = 800;
    scope.height = 600;

    var game = null;

    /* Game methods */
    function initGame() {
      game = new Phaser.Game(scope.width, scope.height, Phaser.AUTO);

      game.state.add('Boot', BootState);
      game.state.add('Preload', PreloadState);
      game.state.add('Menu', MenuState);
      game.state.add('Play', PlayState);
      game.state.add('GameOver', GameOverState);

      game.state.start('Boot');
    }

    /* Public methods */
    scope.start = function() {
      initGame();
    }
  }(Game));

  return Game;
});
