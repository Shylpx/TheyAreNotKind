define(
  ['strategies/movement', 'strategies/shot', 'strategies/impact', 'strategies/effect', 'loaders/image_loader'],
  function(MovementInterface, ShotInterface, ImpactInterface, EffectInterface, ImageLoader) {

  function CustomizationMenu(game, inventory, images) {
    var scope = this;

    this.game = game;
    this.inventory = inventory;
    this.images = images;

    var STRATEGIES = [MovementInterface, ShotInterface, ImpactInterface, ImpactInterface, EffectInterface]; // TODO Cambiar la estrategia central

    var menu = null;

    this.init = function() {
      var config_button = this.game.add.button(
        this.game.world.width - 10,
        this.game.world.height - 10,
        images.button.image,
        this.pause,
        this,
        2, 1, 0
      );
      config_button.anchor.setTo(1, 1);
    }

    function resume() {
      scope.inventory.holes.forEach(function(gem, index) {
        var type = gem == null ? 'default' : scope.inventory.gems[gem.index];
        STRATEGIES[index]().setStrategy(type);
      });

      menu.destroy();

      scope.game.paused = false;
    }

    this.pause = function() {
      if (this.game.paused)
        return;

      // TODO Mostrar las gemas de colores (las conseguidas) y permitir el drag and drop y guardar la configuración
      this.game.paused = true;

      menu = this.game.add.group();
      menu.x = this.game.world.centerX;
      menu.y = this.game.world.centerY;

      var background = menu.create(0, 0, images.background);
      background.anchor.setTo(0.5, 0.5);

      inventory.create(menu);

      var resume_button = this.game.add.button(0, 105, images.apply_button.image, resume, this, 2, 1, 0);
      resume_button.anchor.setTo(0.5, 0.5);
      menu.add(resume_button);
    }
  }
  // TODO Hacer que se asigne una estrategia al salir, con als gemas colocadas
  return CustomizationMenu;
});
