define(['phaser.min'], function(_) {

  function Inventory(game, images, font) {
    var scope = this;

    this.game = game;
    this.images = images;
    this.font = font;
    this.gems = ['fire', 'water', 'wind']
    this.holes = [null, null, null, null, null];

    var HOLES_POSITION = [
      { x: -75, y: -75, icon: 'movement', angle: Math.PI * 0.5 },
      { x: 75, y: -75, icon: 'shot', angle: Math.PI },
      { x: 0, y: 0, icon: 'special' },
      { x: -75, y: 75, icon: 'impact', angle: 0 },
      { x: 75, y: 75, icon: 'effect', angle: -Math.PI * 0.5 }
    ]
    var POOLS_POSITION = [
      { x: -75, y: 180 },
      { x: 0, y: 180 },
      { x: 75, y: 180 }
    ]
    var HOLE_SIZE = 40;
    var DRAG_DELAY = 20; // ms
    // TODO Poner botón de pausa
    var gems_pool = [ // TODO Debe inicializarse a partir de gems al crearse si no existe ya
      { type: 'fire', count: 4 }, // TODO Debe ser 0
      { type: 'water', count: 4 }, // TODO Debe ser 0
      { type: 'wind', count: 4 } // TODO Debe ser 0
    ];
    // TODO DESROMPER TANTO LO DE ATRAVESAR ENEMIGOS
    var menu_group = null;
    var gems_group = null; // TODO Usar para las gemas y otro grupo para el tanque, poniendo el de las gemas arriba
    // TODO Además, subir al toplas gemas al draggearlas, para quese muestren por encima de todo
    var drag_event = null;
    var selected_hole = null;
    // TODO El quemado debe actuar más rápido
    this.init = function() {
      // TODO Mirar si poner aquí lo necesario para el reinicio
    }

    this.addGem = function(gem) {
      gems_pool.find(function(pool) { return pool.type == gem.data.type; }).count += 1;
    }

    this.create = function(menu) {
      menu_group = menu;

      scope.holes.forEach(function(gem, index) {
        // TODO Poner en un grupo, y hacer que tenga mas precediencia de input en el momento del drag, y luego quitarsela, para que se puedan quitar las gemas pulsando y funcione el drag and drop
        var hole = menu.create(HOLES_POSITION[index].x, HOLES_POSITION[index].y, images.hole);
        hole.anchor.setTo(0.5, 0.5);

        if (HOLES_POSITION[index].angle != undefined) {
          var hole_arrow = menu.create(HOLES_POSITION[index].x, HOLES_POSITION[index].y, images.hole_arrow);
          hole_arrow.rotation = HOLES_POSITION[index].angle;
          hole_arrow.anchor.setTo(0.5, 0.5);
        }

        var icon = menu.create(HOLES_POSITION[index].x, HOLES_POSITION[index].y, images[HOLES_POSITION[index].icon]);
        icon.anchor.setTo(0.5, 0.5);

        hole.inputEnabled = true;
        hole.events.onInputOver.add(function(hole) { selected_hole = index; });
        hole.events.onInputOut.add(function() { selected_hole = null; });

        if (gem == null)
          return;

        gem.sprite = menu.create(HOLES_POSITION[index].x, HOLES_POSITION[index].y, images[gems_pool[gem.index].type]);
        gem.sprite.anchor.setTo(0.5, 0.5);

        gem.sprite.inputEnabled = true;
        gem.sprite.input.useHandCursor = true;

        gem.sprite.events.onInputUp.add(function() {
          var pool = gems_pool[gem.index];
          pool.label.text = ++pool.count;

          if (pool.count > 0) {
            pool.sprite.inputEnabled = true;
            pool.sprite.input.useHandCursor = true;
          }

          gem.sprite.destroy();
          scope.holes[index] = null;
        });
      });

      // TODO Mostrar otra imagen en gris si la piscina tiene 0
      gems_pool.forEach(function(pool, index) {
        var background = menu.create(POOLS_POSITION[index].x, POOLS_POSITION[index].y, images[pool.type])
        background.anchor.setTo(0.5, 0.5);

        // TODO Poner en una función
        pool.sprite = menu.create(POOLS_POSITION[index].x, POOLS_POSITION[index].y, images[pool.type])
        pool.sprite.anchor.setTo(0.5, 0.5);

        pool.sprite.data.index = index;

        // TODO Manejar los eventos del drag and drop por separado, ya que si está pausado no va
        pool.sprite.events.onInputDown.add(getGem);
        pool.sprite.events.onInputUp.add(dropGem);

        if (pool.count > 0) {
          pool.sprite.inputEnabled = true;
          pool.sprite.input.useHandCursor = true;
        }

        pool.label = game.add.text(POOLS_POSITION[index].x, POOLS_POSITION[index].y, pool.count, scope.font, menu);
        pool.label.anchor.setTo(0.5, 0.5);
      })
    }
    // TODO El agujero central está bloqueado hasta que se llenen los demás, con animación

    // TODO Refactor and complete
    function getGem(gem) {
      var pool = gems_pool[gem.data.index];
      pool.label.text = --pool.count;

      drag_event = setInterval(function() {
        coordinates = scope.game.input.getLocalPosition(menu_group, scope.game.input);
        gem.position.setTo(coordinates.x, coordinates.y);
      }, DRAG_DELAY);
    }

    // TODO Refactor and complete
    function dropGem(gem) {
      clearInterval(drag_event);

      if (selected_hole == null) { // TODO Refactorizar numeración
        var pool = gems_pool[gem.data.index]
        pool.label.text = ++pool.count;
      }
      else { // TODO Refactorizar creación de gemas
        var new_gem = menu_group.create(HOLES_POSITION[selected_hole].x, HOLES_POSITION[selected_hole].y, images[gems_pool[gem.data.index].type])
        new_gem.anchor.setTo(0.5, 0.5);

        var last_gem = scope.holes[selected_hole];

        scope.holes[selected_hole] = {
          index: gem.data.index,
          sprite: new_gem
        };

        new_gem.inputEnabled = true;
        new_gem.input.useHandCursor = true;
        new_gem.input.priorityID = 1;
        new_gem.events.onInputDown.add(function() {
          var pool = gems_pool[gem.data.index];
          pool.label.text = ++pool.count;

          if (pool.count > 0) {
            pool.sprite.inputEnabled = true;
            pool.sprite.input.useHandCursor = true;
          }

          new_gem.destroy();
          scope.holes[selected_hole] = null;
        });

        var current_pool = gems_pool[gem.data.index];
        if (current_pool.count == 0)
          current_pool.sprite.inputEnabled = false;

        if (last_gem != null) {
          var pool = gems_pool[last_gem.index];
          pool.label.text = ++pool.count;
          pool.sprite.inputEnabled = true;
          pool.sprite.input.useHandCursor = true;
          last_gem.sprite.destroy();
        }
      }

      gem.position.setTo(POOLS_POSITION[gem.data.index].x, POOLS_POSITION[gem.data.index].y);
    }
  }
  // TODO Mostrar en el tanque las gemas seleccionadas
  // TODO Mostrar la imagen del tanque en el menú
  return Inventory;
});
