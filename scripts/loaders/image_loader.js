define(function() {

  var ImageLoader = {};

  (function(scope) {
    scope.IMAGES = {
      background: {
        play: 'background',
        menu: 'menu_background',
        game_over: 'end_background'
      },
      menu: { play: { image: 'play', width: 100, height: 100 } },
      game_over: { restart: { image: 'restart', width: 70, height: 70 } },
      tank: {
        body: 'tank_body',
        cannon: 'tank_cannon',
        cannon_copy: 'tank_cannon_copy',
        bullet: 'bullet'
      },
      enemies: { standart: 'enemy' },
      gems: {
        fire: 'fire_gem',
        water: 'water_gem',
        wind: 'wind_gem',
        hole: 'hole',
        hole_arrow: 'hole_arrow',
        movement: 'movement_icon',
        shot: 'shot_icon',
        special: 'special_icon',
        impact: 'impact_icon',
        effect: 'effect_icon'
      },
      customization: {
        background: 'customization_background',
        apply_button: { image: 'apply_config', width: 60, height: 33 },
        button: { image: 'config', width: 50, height: 50 }
      }
    };

    var IMG_PATH = 'assets/img/';
    var DEFAULT_FORMAT = 'svg';
    var FORMATS = {
      background: { play: 'png', menu: 'png', game_over: 'png' }
    };

    scope.load = function(game) {
      for (var object in scope.IMAGES) {
        for (var image in scope.IMAGES[object]) {
          data = scope.IMAGES[object][image];
          format = (FORMATS[object] || {})[image] || DEFAULT_FORMAT

          if (typeof(data) == 'string')
            game.load.image(data, IMG_PATH + data + '.' + format);
          else
            game.load.spritesheet(data.image, IMG_PATH + data.image + '.' + format, data.width, data.height);
        }
      }
    };
  }(ImageLoader));

  return ImageLoader;
});
