define(['utils/colors'], function(Colors) {

  var FontLoader = {};

  (function(scope) {
    scope.FONTS = {
      inventory: { font: '25px Kurale', fill: Colors.WHITE, stroke: Colors.BLACK, strokeThickness: 1, align: 'center' },
      score: { font: '50px Kurale', fill: Colors.BLACK, stroke: Colors.WHITE, strokeThickness: 2, align: 'left' },
      title: { font: '70px Kurale', fill: Colors.BLACK, stroke: Colors.WHITE, strokeThickness: 4, align: 'center' },
      total_score: { font: '40px Kurale', fill: Colors.BLACK, stroke: Colors.WHITE, strokeThickness: 4, align: 'center' }
    };

    scope.families = function() {
      return Object.keys(scope.FONTS).map(function(font) {
        return font.charAt(0).toUpperCase() + font.slice(1);
      });
    };
  }(FontLoader));

  return FontLoader;
});
