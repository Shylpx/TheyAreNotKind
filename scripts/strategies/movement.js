define(function() {
  // TODO Gestionar las balas que han sido lanzadas ya al cambiar de interfaz. La estrategia se debe
  //      aplicar a la bala, no aplicar a todas las balas vivas.
  function MovementInterface(game, tank, bullets) {
    var instance = MovementInterface.prototype._singletonInstance || this;

    instance.game = game || instance.game;
    instance.tank = tank || instance.tank;
    instance.bullets = bullets || instance.bullets;

    if (bullets != undefined)
      initData(bullets);

    if (MovementInterface.prototype._singletonInstance)
      return MovementInterface.prototype._singletonInstance;

    var scope = MovementInterface.prototype._singletonInstance = this;

    var currentStrategy = defaultStrategy;

    /* Initialize */
    function initData(bullets) {
      bullets.forEach(function(bullet) {
        initBulletData(bullet);
        bullet.events.onKilled.add(function() { initBulletData(bullet); }, this);
      });
    }

    function initBulletData(bullet) {
      bullet.data.oscilation = { init: null, direction: 0.5 };
      bullet.data.spiral = { next: 0, turn: 0 };
    }

    /* Logic */
    this.move = function() {
      currentStrategy();0
    }

    /* Strategies */
    function defaultStrategy() {}
    // TODO Arreglar los disparos que cambian la velocidad, pues al rebotar hace lo que le da la gana
    var SPIRAL_VELOCITY = 50;
    var SPIRAL_RATE = 100;
    var ANGLE_INCREMENT = Math.PI / 6;
    var ANGLE_VARIATION = Math.PI / 60;
    var PERIOD = 2 * Math.PI;
    function spiralStrategy() {
      scope.bullets.forEachAlive(function(bullet) {
        if (bullet.data.spiral.next > scope.game.time.now)
          return;

        bullet.data.spiral.next = scope.game.time.now + SPIRAL_RATE;

        var angle = (Math.atan2(bullet.data.direction.x, bullet.data.direction.y) + ANGLE_INCREMENT - ANGLE_VARIATION * bullet.data.spiral.turn) % PERIOD;
        bullet.data.direction.x = Math.sin(angle);
        bullet.data.direction.y = Math.cos(angle);

        var velocity = bullet.data.velocity + SPIRAL_VELOCITY * bullet.data.spiral.turn;
        bullet.body.velocity.x = bullet.data.direction.x * velocity;
        bullet.body.velocity.y = bullet.data.direction.y * velocity;

        bullet.data.spiral.turn += 1;
      });
    }

    var OSCILATION_VELOCITY = 300;
    var OSCILATION_RATE = 400;
    var COS_PERIOD = 2 * Math.PI;
    function oscilationStrategy() {
      scope.bullets.forEachAlive(function(bullet) {
        if (!bullet.data.oscilation.init)
          bullet.data.oscilation.init = scope.game.time.now;

        var direction = Math.cos(((scope.game.time.now - bullet.data.oscilation.init) % OSCILATION_RATE) * COS_PERIOD / OSCILATION_RATE);

        bullet.body.velocity.x = bullet.data.direction.x * bullet.data.velocity + bullet.data.direction.y * direction * OSCILATION_VELOCITY;
        bullet.body.velocity.y = bullet.data.direction.y * bullet.data.velocity - bullet.data.direction.x * direction * OSCILATION_VELOCITY;
      })
    }

    var MISSILE_INCREMENT = 1.15;
    function missileStrategy() {
      scope.bullets.setAll('body.angularVelocity', 0);
      scope.bullets.forEachAlive(function(bullet) {
        bullet.body.velocity.x = bullet.body.velocity.x * MISSILE_INCREMENT;
        bullet.body.velocity.y = bullet.body.velocity.y * MISSILE_INCREMENT;
      })
    }

    /* Controller */
    this.setStrategy = function(strategy) {
      switch(strategy) {
        case 'fire':
          currentStrategy = spiralStrategy;
          break;
        case 'water':
          currentStrategy = oscilationStrategy;
          break;
        case 'wind':
          currentStrategy = missileStrategy;
          break;
        default:
          currentStrategy = defaultStrategy;
          break;
      }
    }
  }

  return MovementInterface;
});
