define(['utils/math'], function(MathUtils) {

  function ImpactInterface(game, enemies) {
    var instance = ImpactInterface.prototype._singletonInstance || this;

    instance.game = game || instance.game;
    instance.enemies = enemies || instance.enemies;

    if (ImpactInterface.prototype._singletonInstance)
      return ImpactInterface.prototype._singletonInstance;

    var scope = ImpactInterface.prototype._singletonInstance = this;

    var currentStrategy = defaultStrategy;
    var currentCollision = defaultCollision;

    /* Logic */
    this.impact = function(enemy, bullet) {
      currentStrategy(enemy, bullet);
    }

    this.collision = function(bullets, enemies, callback) {
      currentCollision(bullets, enemies, callback);
    }

    /* Configuration */
    function defaultConfiguration(enemies) {
      enemies.setAll('body.enabled', true);
    }

    function pierceConfiguration(enemies) {
      enemies.setAll('body.enabled', false);
    }

    /* Collisions */
    function defaultCollision(bullets, enemies, callback) {
      scope.game.physics.arcade.overlap(bullets, enemies, callback);
    }

    function bounceCollision(bullets, enemies, callback) {
      scope.game.physics.arcade.collide(bullets, enemies, callback);
    }

    /* Strategies */
    function defaultStrategy(_, bullet) {
      bullet.kill(); // TODO Animación de destrucción de bala
    }

    var BURST_RADIUS = 80;
    var BURST_DAMAGE_RATIO = 0.8;
    function burstStrategy(enemy, bullet) {
      // TODO Animación de explosión
      burned = [];
      enemy.data.horde.forEachAlive(function(enemy) {
        if (MathUtils.distanceBetween2Points(enemy.body, bullet.body) > BURST_RADIUS)
          return;
        burned.push(enemy);
      });

      burned.forEach(function(enemy) {
        enemy.hit(bullet.data.damage * BURST_DAMAGE_RATIO);
      });

      bullet.kill(); // TODO Animación de destrucción de bala
    }

    function bounceStrategy(_, _) {}

    PIERCE_DAMAGE_REDUCTION = 0.7;
    PIERCE_MIN_DAMAGE = 4;
    function pierceStrategy(_, bullet) {
      bullet.data.damage = Math.max(bullet.data.damage * PIERCE_DAMAGE_REDUCTION, PIERCE_MIN_DAMAGE);
      // TODO Hacer que cambie de color al impactar
    }

    /* Controller */
    this.setStrategy = function(strategy) {
      defaultConfiguration(this.enemies);

      switch(strategy) {
        case 'fire':
          currentStrategy = burstStrategy;
          currentCollision = defaultCollision;
          break;
        case 'water':
          currentStrategy = bounceStrategy;
          currentCollision = bounceCollision;
          break;
        case 'wind':
          currentStrategy = pierceStrategy;
          currentCollision = defaultCollision;
          pierceConfiguration(this.enemies);
          break;
        default:
          currentStrategy = defaultStrategy;
          currentCollision = defaultCollision;
          break;
      }
    }
  }

  return ImpactInterface;
});
