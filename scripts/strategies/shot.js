define(['loaders/image_loader'], function(ImageLoader) {

  function ShotInterface(game, tank, bullets) {
    var instance = ShotInterface.prototype._singletonInstance || this;

    instance.game = game || instance.game;
    instance.tank = tank || instance.tank;
    instance.bullets = bullets || instance.bullets;

    if (ShotInterface.prototype._singletonInstance)
      return ShotInterface.prototype._singletonInstance

    var scope = ShotInterface.prototype._singletonInstance = this;

    var CENTER = { x: this.game.world.width / 2, y: this.game.world.height / 2 };
    var INIT_ROTATION = 0;
    var CENTER_OFFSET = 50;
    var FIRE_RATE = { default: 80, fire: 70, wind: 30, water: 140 };
    var VELOCITY = { default: 200, fire: 300, wind: 180, water: 150 };
    var ANGULAR_VELOCITY = 400;
    var DAMAGE = 25;

    var next_fire = 0;
    var fire_rate = FIRE_RATE.default;
    var velocity = VELOCITY.default;

    var currentStrategy = defaultStrategy;

    /* Logic */
    this.fire = function() {
      if (this.game.input.activePointer.isUp)
        return;

      if (this.game.time.now < next_fire || this.bullets.countDead() == 0)
        return;

      next_fire = this.game.time.now + fire_rate;

      currentStrategy();
    }

    function unitaryVector(rotation, dir_x, dir_y, invert) {
      return {
        x: dir_x * Math.cos(rotation) * (invert ? Math.tan(rotation) : 1),
        y: dir_y * Math.sin(rotation) / (invert ? Math.tan(rotation) : 1)
      }
    }

    function fireBullet(bullet, options = {}) {
      bullet.rotation = scope.tank.object.rotation;

      var dir_x = options.dir_x || 1;
      var dir_y = options.dir_y || 1;

      var p_offset = options.p_offset || 0;
      var v_offset = options.v_offset ? options.v_offset * (2 * Math.random() - 1) : 0;

      var base_rotation = bullet.rotation - scope.tank.INIT_ROTATION;
      var p_rotation = base_rotation + p_offset;
      var v_rotation = base_rotation + v_offset;

      var p_vector = unitaryVector(p_rotation, dir_x, dir_y, options.invert);
      var v_vector = unitaryVector(v_rotation, dir_x, dir_y, options.invert);

      bullet.data.direction = v_vector;
      bullet.data.velocity = velocity;
      bullet.data.damage = DAMAGE;

      bullet.reset(CENTER.x + p_vector.x * CENTER_OFFSET, CENTER.y + p_vector.y * CENTER_OFFSET);

      bullet.body.angularVelocity = ANGULAR_VELOCITY;
      bullet.body.velocity.x = v_vector.x * velocity;
      bullet.body.velocity.y = v_vector.y * velocity;
    }

    /* Strategies */
    function defaultStrategy() {
      fireBullet(scope.bullets.getFirstDead());
    }

    var PILLARS_OFFSET = Math.PI / 27;
    function pillarStrategy() {
      if (scope.bullets.countDead() < 2)
        return;

      fireBullet(scope.bullets.getFirstDead(), { p_offset: -PILLARS_OFFSET });
      fireBullet(scope.bullets.getFirstDead(), { p_offset: PILLARS_OFFSET });
    }

    var CROSS_DIRECTIONS = [-1, 1];
    function crossStrategy() {
      for (x in CROSS_DIRECTIONS) {
        for (y in CROSS_DIRECTIONS) {
          if (scope.bullets.countDead() == 0)
            return;

          fireBullet(scope.bullets.getFirstDead(), { dir_x: CROSS_DIRECTIONS[x], dir_y: CROSS_DIRECTIONS[y], invert: (CROSS_DIRECTIONS[x] * CROSS_DIRECTIONS[y]) == -1 });
        }
      }
    }

    var BREATH_OFFSET = Math.PI / 9;
    function breathStrategy() {
      fireBullet(scope.bullets.getFirstDead(), { v_offset: BREATH_OFFSET });
    }

    /* Controller */
    this.setStrategy = function(strategy) {
      switch(strategy) {
        case 'fire':
          fire_rate = FIRE_RATE.fire;
          velocity = VELOCITY.fire;
          currentStrategy = pillarStrategy;
          scope.tank.destroyAdditionalCannons();
          break;
        case 'water':
          fire_rate = FIRE_RATE.water;
          velocity = VELOCITY.water;
          currentStrategy = crossStrategy;
          scope.tank.createAdditionalCannons();
          break;
        case 'wind':
          fire_rate = FIRE_RATE.wind;
          velocity = VELOCITY.wind;
          currentStrategy = breathStrategy;
          scope.tank.destroyAdditionalCannons();
          break;
        default:
          fire_rate = FIRE_RATE.default;
          velocity = VELOCITY.default;
          currentStrategy = defaultStrategy;
          scope.tank.destroyAdditionalCannons();
          break;
      }
    }
  }

  return ShotInterface;
});
