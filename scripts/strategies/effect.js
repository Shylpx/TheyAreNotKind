define(function() {

  function EffectInterface(game) {
    var instance = EffectInterface.prototype._singletonInstance || this;

    instance.game = game || instance.game;

    if (EffectInterface.prototype._singletonInstance)
      return EffectInterface.prototype._singletonInstance;

    var scope = EffectInterface.prototype._singletonInstance = this;

    // TODO Gestionar todos los cambios de estrategias, para que los efectos causados se mantengan y no haya conflictos entre ellos
    // TODO Podría hacerse con una función de cada efecto, que eliminase sus consecuencias, en caso de que pudiesen entrar en conflicto.
    var currentStrategy = defaultStrategy;

    /* Logic */
    this.effect = function(enemy, bullet) {
      currentStrategy(enemy, bullet);
    }

    /* Strategies */
    function defaultStrategy(_, _, _) {}
    // TODO Los enemigos se colorean de una tonalidad más clara según el porcentaje de vida que les queda
    // TODO Si al volver alguna bala choca contra el tanque, se ve como un escudo aparece, y la bala desaparece con su animación
    // TODO Límite a la quemadura, no se puede quemar mientras estás quemado
    // TODO Refactorizar todas las constantes de configuración en una clase que permita modificarlo de forma interactiva
    var BURN_TIME = 500;
    var BURN_DAMAGE_RATIO = 0.5;
    var BURN_PULSES = 5;
    function burnStrategy(enemy, bullet) {
      if (enemy.data.effect.event) {
        // TODO Refactor
        scope.game.time.events.remove(enemy.data.effect.event);
        delete enemy.data.effect['event']
      }

      enemy.data.effect.burn_pulses = 0;

      enemy.data.effect.event = scope.game.time.events.loop(BURN_TIME, function() {
        if (!enemy.alive) {
          // TODO Refactor
          // TODO, es el enemigo, al ser destruido, el que debe destruir los eventos que tenga
          //scope.game.time.events.remove(enemy.data.effect.event);
          //delete enemy.data.effect['event'] // TODO Necesario ?, mirar que pasa cuando se borra el evento
          return;
        }

        ++enemy.data.effect.burn_pulses;

        if (enemy.data.effect.burn_pulses >= BURN_PULSES) {
          scope.game.time.events.remove(enemy.data.effect.event);
          delete enemy.data.effect['event'] // TODO Necesario ?, mirar que pasa cuando se borra el evento
        }

        enemy.hit(bullet.data.damage * BURN_DAMAGE_RATIO, undefined, undefined, function() {
          scope.game.time.events.remove(enemy.data.effect.event);
          delete enemy.data.effect['event'] // TODO Necesario ?, mirar que pasa cuando se borra el evento
        });
      });
    }

    // TODO Ubicar función bajo Easing
    function wiggle(aProgress, aPeriod1, aPeriod2) {
      var current1 = aProgress * 2 * Math.PI * aPeriod1;
      var current2 = aProgress * (2 * Math.PI * aPeriod2 + Math.PI / 2);

      return Math.sin(current1) * Math.cos(current2);
    }

    var FREEZE_DURATION = 700;
    var SHAKE_DURATION = 300;
    var SHAKE_PERIOD = 1;
    // TODO Está un poco roto, límite de congelación ?
    function freezeStrategy(enemy, _, _) {
      enemy.body.velocity.setTo(0, 0);

      if (enemy.data.effect.tween)
        enemy.data.effect.tween.stop();

      enemy.data.effect.tween = scope.game.add.tween(enemy);

      // TODO Revisar y no usar números, y ver si usar un TWEEN para la y
      enemy.data.effect.tween.to({ x: enemy.body.x }, SHAKE_DURATION, function (k) {
        return wiggle(k, SHAKE_PERIOD, SHAKE_PERIOD);
      }, true, FREEZE_DURATION).onComplete.add(function() {
        enemy.body.velocity.setTo(enemy.data.velocity.x, enemy.data.velocity.y);
      });
    }

    var PUSH_DURATION = 200;
    var POWER_PROPAGATION = 0.08;
    // TODO Cada bala sólo puede empujar a un cuadrado una vez
    function pushStrategy(enemy, bullet, _) {
      scope.game.add.tween(enemy).to({
        x: enemy.body.x + bullet.body.velocity.x * POWER_PROPAGATION,
        y: enemy.body.y + bullet.body.velocity.y * POWER_PROPAGATION
      }, PUSH_DURATION, Phaser.Easing.Quartic.Out, true).onComplete.add(function() {
        // TODO Refactorizar como una función de "enemy", para establecer la dirección
        var rotation = enemy.data.attack();
        if (Math.abs(rotation - enemy.rotation) > Math.PI)
          rotation += (enemy.rotation < 0 ? -1 : 1) * 2 * Math.PI;
        scope.game.add.tween(enemy).to({ rotation: rotation }, PUSH_DURATION, Phaser.Easing.Quartic.Out, true)
      });
    }

    /* Controller */
    this.setStrategy = function(strategy) {
      switch(strategy) {
        case 'fire':
          currentStrategy = burnStrategy;
          break;
        case 'water':
          currentStrategy = freezeStrategy;
          break;
        case 'wind':
          currentStrategy = pushStrategy;
          break;
        default:
          currentStrategy = defaultStrategy;
          break;
      }
    }
  }

  return EffectInterface;
});
