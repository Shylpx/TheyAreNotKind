define(function() {

  function Scoreboard(game, font) {
    if (Scoreboard.prototype._singletonInstance)
      return Scoreboard.prototype._singletonInstance;

    var scope = Scoreboard.prototype._singletonInstance = this;

    this.game = game;
    this.font = font;

    var X_OFFSET = 80;

    var score = null;
    var total = null;

    this.init = function() {
      total = 0;

      score = scope.game.add.text(X_OFFSET, this.game.world.height, total, this.font);
      score.anchor.setTo(0, 1);
    }

    this.addPoints = function(points) {
      total += points;
      score.setText(total);
    }

    this.totalScore = function() {
      return total;
    }
  }

  return Scoreboard;
});
