define(['phaser.min', 'strategies/shot', 'strategies/movement'], function(_, ShotInterface, MovementInterface) {

  function Bullets(game, parent) {
    var scope = this;

    this.game = game;
    this.object = null;
    this.parent = parent;

    var BULLETS_LIMIT = 200;

    var shot_strategy = null;
    var movement_strategy = null;

    /* Create and init objects */
    this.init = function(images, init_rotation) {
      this.object = this.game.add.group();
      this.object.enableBody = true;
      this.object.physicsBodyType = Phaser.Physics.ARCADE;
      this.object.createMultiple(BULLETS_LIMIT, 'bullet');

      this.object.setAll('checkWorldBounds', true);
      this.object.setAll('outOfBoundsKill', true);
      this.object.setAll('anchor', { x: 0.5, y: 0.5 });
      this.object.setAll('body.bounce', { x: 1, y: 1 });

      // TODO Strategies sat by gems
      shot_strategy = new ShotInterface(this.game, this.parent, this.object);
      shot_strategy.setStrategy();

      // TODO Poner límite al empuje de viento, para que el modo misil no lo envíe al infinito
      // TODO No me gusta que el modo full fuego no tenga mucha coherencia por la espiral, el ángulo debería ser de 4 para el fuego, pero de entre 6 y 8 para el agua y 5 para el viento
      movement_strategy = new MovementInterface(this.game, this.parent, this.object);
      movement_strategy.setStrategy();
    }

    /* Update objects */
    this.update = function() {
      moveBullets();
      fireBullet();
    }

    function moveBullets() {
      movement_strategy.move();
    }

    function fireBullet() {
      shot_strategy.fire();
    }
  }

  return Bullets;
});
