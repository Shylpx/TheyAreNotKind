define(
  ['phaser.min', 'objects/scoreboard', 'utils/colors'],
  function(_, Scoreboard, Colors) {

  Enemy = function(game, images) {
    var scope = this;

    this.game = game;

    var POINTS = 10;
    var LIFE_BAR_THICKNESS = 4;
    var LIFE_BAR_RADIUS = 18;
    var FULL_ANGLE = 2 * Math.PI;

    var x = game.rnd.between(0, game.width);
    var y = Math.random() > 0.5 ? 0 : game.world.height;
    // TODO Añadir el resto de cosas de los enemies, y usar estas variables en vez de "data"
    var last_life = null;
    var life_bar = null;

    this.hit = function(power, callback, alive_callback, death_callback) {
      this.hurt(power);

      this.runCallback(callback);

      if (!this.alive) {
        this.runCallback(death_callback);
        return;
      }

      if (this.data.life <= 0) {
        this.runCallback(death_callback);

        Scoreboard().addPoints(POINTS);
        this.destroy();
      }
      else
        this.runCallback(alive_callback);
    }

    this.runCallback = function(callback) {
      if (callback != undefined)
        callback();
    }
    // TODO Arreglar la fuente, y que no sea necesario conectarse a internet
    // TODO Aumentar la velocidad de giro del tanque
    // TODO Controles de flechas
    this.hurt = function(power) {
      this.data.life = Math.max(this.data.life - power, 0);
    }

    this.printLife = function() {
      if (this.data.life == last_life || this.data.life == 100)
        return; // TODO Usar la constante de Enemies una vez refactorizado

      last_life = this.data.life;

      if (life_bar != null)
        life_bar.reset();

      life_bar = this.game.add.graphics(0, 0);
      life_bar.lineStyle(LIFE_BAR_THICKNESS, Colors.RED);
      life_bar.arc(0, 0, LIFE_BAR_RADIUS, 0, FULL_ANGLE);
      life_bar.lineStyle(4, Colors.GREEN);
      life_bar.arc(0, 0, LIFE_BAR_RADIUS, 0, FULL_ANGLE * this.data.life / 100); // TODO Usar la constante de Enemies una vez refactorizado
      // TODO El impacto de las balas está afectando a la barra de vida también, debe ser impedido

      this.addChild(life_bar);
    }

    /* Init enemy */
    Phaser.Sprite.call(this, game, x, y, images.standart);
    this.data.life = 100; // TODO Eliminar
    this.printLife();
  }

  Enemy.prototype = Object.create(Phaser.Sprite.prototype);
  Enemy.prototype.constructor = Enemy;

  return Enemy;
});
