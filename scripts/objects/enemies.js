define(
  ['phaser.min', 'strategies/impact', 'strategies/effect', 'enemies/enemy'],
  function(_, ImpactInterface, EffectInterface, Enemy) {
  // TODO Posibilidad de gestionar distintos tipos de enemigos
  // TODO Sistema para cambiar los parámetros del tanque (velocidad, rotación y tal, con un panel)
  // https://phaser.io/examples/v2/sprites/extending-sprite-demo-2
  function Enemies(game, target, gems, images) {
    var scope = this;

    this.game = game;
    this.target = target;
    this.gems = gems;
    this.images = images;
    this.object = null;

    var DAMAGE = 5;
    var LIFE = 100;
    var SPEED = 70;
    var CREATION_RATE = 1200;
    // TODO Ir aumentando la vida de lso enemigos, el número de apariciones, su velocidad, su daño y la puntuación con el tiempo
    var impact_strategy = null;
    var effect_strategy = null;
    var next_enemy = 0;
    // TODO en el game over, poner a camara lenta la animación de explosión de enemigo, (desactivando los controles)
    /* Create and init objects */
    this.init = function() {
      this.object = this.game.add.group();
      this.object.enableBody = true;
      this.object.physicsBodyType = Phaser.Physics.ARCADE;

      // TODO Strategies sat by gems
      impact_strategy = new ImpactInterface(this.game, this.object);
      impact_strategy.setStrategy();

      effect_strategy = new EffectInterface(this.game);
      effect_strategy.setStrategy();
    }

    // TODO Al mezclar el empuje (effect wind), con el rebote (impact water), se está asignando como dirección del tween, la nueva dirección de la bala, y no la antigua, debe modificarse esto.

    /* Update objects */
    this.update = function() {
      life();
      create();
      collision();
      action();
    }

    function life() {
      scope.object.forEach(function(enemy) {
        enemy.printLife();
      });
    }

    function create() {
      if (scope.game.time.now < next_enemy)
        return;

      next_enemy = scope.game.time.now + CREATION_RATE;

      var enemy = scope.object.add(new Enemy(scope.game, scope.images)); // TODO Más tipos de enemigos
      enemy.anchor.setTo(0.5, 0.5);

      // TODO Refactor as callback
      enemy.data.attack = function() {
        return scope.game.physics.arcade.moveToObject(enemy, scope.target.object, SPEED);
      };
      enemy.rotation = enemy.data.attack();
      enemy.body.immovable = true;

      enemy.data.horde = scope.object;
      enemy.data.velocity = { x: enemy.body.velocity.x, y: enemy.body.velocity.y };
      enemy.data.life = LIFE;
      enemy.data.effect = {};

      // TODO Refactorizar par que sólo se llame cuando se destruye por muerte por bala y poner en otro lado (una función)
      enemy.events.onDestroy.add(function(enemy) { scope.gems.confirmKill(enemy); }, this);
    }

    function collision() {
      // TODO Complete collisions
      scope.game.physics.arcade.overlap(scope.target.object, scope.object, function(tank, enemy) {
        scope.target.hit(DAMAGE);
        enemy.destroy();
      });

      impact_strategy.collision(scope.target.bullets.object, scope.object, function(bullet, enemy) {
        enemy.hit(
          bullet.data.damage,
          function() { impact_strategy.impact(enemy, bullet); },
          function() { effect_strategy.effect(enemy, bullet); }
        );
      });
    }

    function action() {
      // TODO Execute action
    }
  }

  return Enemies;
});
