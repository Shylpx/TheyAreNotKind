define(
  ['phaser.min', 'objects/bullets', 'utils/colors'],
  function(_, Bullets, Colors) {

  function Tank(game, images) {
    var scope = this;

    this.INIT_ROTATION = Math.PI / 2;
    this.game = game;
    this.images = images;
    this.object = null;
    this.bullets = null;

    var LIFE = 100;
    var LIFE_BAR_OFFSET = 35;
    var LIFE_BAR_THICKNESS = 20;
    var LIFE_BAR_RADIUS = 15;
    var BASE_V = 0.025;
    var MAX_V = 0.07;
    var INCREMENT_V = 0.00003;
    var PRECISION = 10;
    var N_ADDITIONAL_CANNONS = 3;
    var ADDITIONAL_CANNONS_ROTATION = [-Math.PI / 2, Math.PI / 2, Math.PI];

    var life = 0;
    var last_life = null;
    var life_bar = null;
    var last_direction = 0;
    var last_time = 0;
    var current_v = 0;
    var additional_cannons = null;

    /* Create and init object */
    this.init = function() {
      /* Initialize data */
      life = LIFE;
      last_life = null;
      last_direction = 0;
      last_time = 0;
      current_v = BASE_V;

      /* Initialize tank */
      this.object = this.game.add.group();
      this.object.x = this.game.world.centerX;
      this.object.y = this.game.world.centerY;

      var cannon = this.object.create(0, 0, images.cannon);
      cannon.anchor.setTo(0.5, 0.5);

      var body = this.object.create(0, 0, images.body);
      body.anchor.setTo(0.5, 0.5);

      this.game.physics.arcade.enable(this.object);

      this.object.setAll('body.immovable', true);

      // TODO Ver y gestionar la zona de colisión del tanque y los enemigos

      /* Initialize bullets */
      this.bullets = new Bullets(this.game, this);
      this.bullets.init(images.bullet);
    }

    this.hit = function(damage) {
      // TODO Game over if the tank lost all its life.
      life -= damage;

      if (life <= 0)
        scope.game.state.start('GameOver');
    }

    // TODO Change the color of the additional cannons
    this.createAdditionalCannons = function() {
      if (additional_cannons)
        return;

      additional_cannons = this.object.createMultiple(N_ADDITIONAL_CANNONS, this.images.cannon_copy, null, true)
      additional_cannons.forEach(function(cannon, index) {
        scope.object.setChildIndex(cannon, 0);
        cannon.anchor.setTo(0.5, 0.5);
        cannon.rotation = ADDITIONAL_CANNONS_ROTATION[index];
      });
    }

    this.destroyAdditionalCannons = function() {
      if (!additional_cannons)
        return;

      additional_cannons.forEach(function(cannon) {
        cannon.destroy();
      });
      additional_cannons = null;
    }

    /* Update object */
    this.update = function() {
      updatePosition();
      updateBullets();
      updateLifeBar();
    }

    function updatePosition() {
      var target = scope.game.physics.arcade.angleToPointer(scope.object) + scope.INIT_ROTATION;

      var total = target - scope.object.rotation;
      if (Math.abs(total) > Math.PI)
        total = (2 * Math.PI - Math.abs(total)) * -Math.sign(total)

      var direction = Math.sign(total);

      var time = scope.game.time.now;
      if (last_direction != direction || total.toFixed(PRECISION) == 0)
        current_v = BASE_V;
      else if (current_v < MAX_V)
        current_v += INCREMENT_V * (time - last_time);

      var rotation = scope.object.rotation + direction * Math.min(current_v, Math.abs(total));
      if (Math.abs(rotation) > Math.PI)
        rotation += 2 * Math.PI * -Math.sign(rotation);
      scope.object.rotation = rotation;

      last_direction = direction;
      last_time = time;
    }
    // TODO Que se guarden las puntuaciones
    function updateBullets() {
      scope.bullets.update();
    }
    // TODO Las gemas que aparecen, deben estar por debajo de los enemigos.
    function updateLifeBar() {
      if (last_life == life)
        return;

      last_life = life;

      if (life_bar != null)
        life_bar.reset();

      life_bar = scope.game.add.graphics(0, 0);
      life_bar.lineStyle(LIFE_BAR_THICKNESS, Colors.RED);
      life_bar.arc(LIFE_BAR_OFFSET, scope.game.world.height - LIFE_BAR_OFFSET, LIFE_BAR_RADIUS, 0, 2 * Math.PI);
      life_bar.lineStyle(LIFE_BAR_THICKNESS, Colors.GREEN);
      life_bar.arc(LIFE_BAR_OFFSET, scope.game.world.height - LIFE_BAR_OFFSET, LIFE_BAR_RADIUS, 0, -2 * Math.PI * life / LIFE, true);
    }
  }

  return Tank;
});
