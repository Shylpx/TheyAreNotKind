define(function() {

  function Gems(game, inventory, images) {
    var scope = this;

    this.game = game;
    this.inventory = inventory;
    this.images = images;
    this.object = null;

    var N_GEMS = 5;
    var GEM_TYPES = ['fire', 'water', 'wind'];
    var SPAWN_BASE = Phaser.Timer.SECOND * 20;
    var ERROR_MARGIN = Phaser.Timer.SECOND * 20;
    var GEM_LIFE = Phaser.Timer.SECOND * 10;
    var ENEMY_KILLS = 20;

    var next_gem = {};
    var gem = null;
    var event = null;

    /* Create and init objects */
    this.init = function() {
      this.object = this.game.add.group();

      scheduleNextGem();
    }

    /* Update objects */
    this.confirmKill = function(enemy) {
      incrementCounter();
      create(enemy.x, enemy.y);
    }

    function incrementCounter() {
      next_gem.kills += 1;
    }

    // TODO Comprobar como va el tiempo, si pausado sigue contando
    function create(x, y) {
      if (scope.game.time.now < next_gem.time || next_gem.kills < ENEMY_KILLS)
        return;

      gem = scope.object.create(x, y, scope.images[next_gem.type])
      gem.anchor.setTo(0.5, 0.5);

      gem.inputEnabled = true;
      gem.input.useHandCursor = true;

      event = game.time.events.add(GEM_LIFE, destroyGem);
      gem.events.onInputDown.add(pickGem);

      gem.data.type = next_gem.type;

      scheduleNextGem();
    }

    // TODO Impedir coger gema con el menú puesto
    function pickGem(gem) {
      scope.inventory.addGem(gem);

      // TODO Animación de cogimiento
      scope.game.time.events.remove(event);
      gem.destroy();
    }

    function destroyGem() {
      // TODO Animación de muerte
      gem.destroy();
    }

    function scheduleNextGem() {
      next_gem = {
        time: scope.game.time.now + SPAWN_BASE + Math.floor(Math.random() * (ERROR_MARGIN + 1)),
        type: GEM_TYPES[Math.floor(Math.random() * GEM_TYPES.length)],
        kills: 0
      };
    }
  }

  return Gems;
});
