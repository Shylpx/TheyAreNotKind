define(['phaser.min'], function(_) {

  var BootState = {

    preload: function() {
      this.game.physics.startSystem(Phaser.Physics.ARCADE);
    },

    create: function() {
      this.game.state.start('Preload');
    },

    update: function() {},

    render: function() {}
  };

  return BootState;
});
