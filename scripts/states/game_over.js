define(
  ['objects/scoreboard', 'loaders/image_loader', 'loaders/font_loader'],
  function(Scoreboard, ImageLoader, FontLoader) {

  var GAME_OVER_STRING = 'Game Over';
  var TOTAL_SCORE_STRING = 'Total Score: ';
  var TOTAL_SCORE_Y_OFFSET = -100;
  var RESTART_Y_OFFSET = 80;

  var GameOverState = {

    preload: function() {
      this.game.add.sprite(0, 0, ImageLoader.IMAGES.background.game_over);
    },

    create: function() {
      var title = this.game.add.text(
        this.game.world.width / 2,
        (this.game.world.height / 2) + TOTAL_SCORE_Y_OFFSET,
        GAME_OVER_STRING,
        FontLoader.FONTS.title
      );
      title.anchor.setTo(0.5, 0.5);

      var score = this.game.add.text(
        this.game.world.width / 2,
        this.game.world.height / 2,
        TOTAL_SCORE_STRING + Scoreboard().totalScore(),
        FontLoader.FONTS.total_score
      );
      score.anchor.setTo(0.5, 0.5);

      var restart = this.game.add.button(
        this.game.world.width / 2,
        (this.game.world.height / 2) + RESTART_Y_OFFSET,
        ImageLoader.IMAGES.game_over.restart.image,
        function() {
          this.game.state.start('Play');
        },
        this,
        2, 1, 0
      );
      restart.anchor.setTo(0.5, 0.5);
    },

    update: function() {},

    render: function() {}
  };

  return GameOverState;
});
