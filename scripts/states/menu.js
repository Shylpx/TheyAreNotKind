define(['loaders/image_loader'], function(ImageLoader) {

  /* Buttons */
  var PLAY_BUTTON_WIDTH = 100;
  var PLAY_BUTTON_HEIGHT = 50;

  /* Menu state */
  var MenuState = {

    preload: function() {
      this.game.add.sprite(0, 0, ImageLoader.IMAGES.background.menu);
    },

    create: function() {
      var play_button = this.game.add.button(
        this.game.world.centerX,
        this.game.world.centerY,
        ImageLoader.IMAGES.menu.play.image,
        function() {
          this.game.state.start('Play');
        },
        this,
        2, 1, 0
      );
      play_button.anchor.setTo(0.5, 0.5);
      // TODO Resto de botones (configuración y salir)
    },

    update: function() {},

    render: function() {}
  };

  return MenuState;
});
