define(['web_font', 'loaders/image_loader', 'loaders/font_loader'], function(_, ImageLoader, FontLoader) {


  var PreloadState = {

    preload: function() {
      ImageLoader.load(this.game);
    },

    create: function() {
      this.game.state.start('Menu');
    },

    update: function() {},

    render: function() {}
  };

  return PreloadState;
});
