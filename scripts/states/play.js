define(
  ['menu/customization', 'menu/inventory', 'objects/tank', 'objects/enemies', 'objects/gems', 'objects/scoreboard', 'loaders/image_loader', 'loaders/font_loader'],
  function(CustomizationMenu, Inventory, Tank, Enemies, Gems, Scoreboard, ImageLoader, FontLoader) {
  // TODO Revisar posibilidad de usar la acceleration para el movimiento del tanque y las balas
  // TODO Utilizar las funciones setTo de las cosas
  var inventory = null;
  var tank = null;
  var gems = null;
  var enemies = null;
  var customization = null;
  var scoreboard = null;

  var PlayState = {

    preload: function() {
      this.game.add.sprite(0, 0, ImageLoader.IMAGES.background.play);
    },

    create: function() {
      inventory = new Inventory(this.game, ImageLoader.IMAGES.gems, FontLoader.FONTS.inventory);
      inventory.init();

      tank = new Tank(this.game, ImageLoader.IMAGES.tank);
      tank.init();

      gems = new Gems(this.game, inventory, ImageLoader.IMAGES.gems);
      gems.init();

      enemies = new Enemies(this.game, tank, gems, ImageLoader.IMAGES.enemies);
      enemies.init();

      customization = new CustomizationMenu(this.game, inventory, ImageLoader.IMAGES.customization);
      customization.init();

      scoreboard = new Scoreboard(this.game, FontLoader.FONTS.score);
      scoreboard.init();
    },

    update: function() {
      tank.update();
      enemies.update();
    },

    render: function() {},

    init: function() {
      WebFontConfig = {
        active: function() { this.game.time.events.add(Phaser.Timer.SECOND, this.createText, this); },
        google: { families: FontLoader.families() }
      }
    }
  };

  return PlayState;
});
