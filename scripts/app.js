requirejs.config({
  baseUrl: 'scripts/lib',
  paths: {
    game: '../game/main',

    'web_font': 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont',

    'objects/tank': '../objects/tank',
    'objects/bullets': '../objects/bullets',
    'objects/enemies': '../objects/enemies',
    'objects/gems': '../objects/gems',
    'objects/scoreboard': '../objects/scoreboard',

    'enemies/enemy': '../objects/enemies/enemy',

    'menu/customization': '../menu/customization',
    'menu/inventory': '../menu/inventory',

    'states/boot': '../states/boot',
    'states/game_over': '../states/game_over',
    'states/menu': '../states/menu',
    'states/play': '../states/play',
    'states/preload': '../states/preload',

    'loaders/image_loader': '../loaders/image_loader',
    'loaders/font_loader': '../loaders/font_loader',

    'strategies/shot': '../strategies/shot',
    'strategies/movement': '../strategies/movement',
    'strategies/impact': '../strategies/impact',
    'strategies/effect': '../strategies/effect',

    'utils/math': '../utils/math',
    'utils/colors': '../utils/colors'
  }
});

requirejs(['game'], function(Game) {
  Game.start();
});
